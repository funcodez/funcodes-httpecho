// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.httpecho;

import static org.refcodes.cli.CliSugar.*;
import static org.refcodes.rest.HttpRestServerSugar.*;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.List;
import java.util.regex.Pattern;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.CliSugar;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.NoneOperand;
import org.refcodes.cli.Option;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.properties.JavaPropertiesBuilder.JavaPropertiesBuilderFactory;
import org.refcodes.properties.JsonPropertiesBuilder.JsonPropertiesBuilderFactory;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.properties.PropertiesBuilderImpl;
import org.refcodes.properties.ResourcePropertiesFactory.ResourcePropertiesBuilderFactory;
import org.refcodes.properties.TomlPropertiesBuilder.TomlPropertiesBuilderFactory;
import org.refcodes.properties.XmlPropertiesBuilder.XmlPropertiesBuilderFactory;
import org.refcodes.properties.YamlPropertiesBuilder.YamlPropertiesBuilderFactory;
import org.refcodes.rest.HttpRestServerSugar;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.time.DateFormat;
import org.refcodes.web.HttpFields;

/**
 * Demo application on how to use command line argument parsing with ease using
 * {@link CliSugar} and on how to set up a simple RESTful service with ease
 * using {@link HttpRestServerSugar}.
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "httpecho";
	private static final String TITLE = "HTTP⮕ECHO";
	private static final String DESCRIPTION = "Small HTTP-Echo server echoing incoming HTTP-Requests back to the client and to the console (see [https://www.metacodes.pro/manpages/httpecho_manpage]).";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES | See [https://www.metacodes.pro/manpages/httpecho_manpage]";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String HTTP_REQUEST_SECTION = "httpRequest";
	private static final String META_DATA_SECTION = "metaData";
	private static final String SERVER_SECTION = "server";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final NoneOperand theNoneArg = none( "Starts with the default configuration." );
		final Option<Integer> thePortArg = intOption( 'p', "port", "port", "Sets the port for the server." );
		final Option<Integer> theMaxConnsArg = intOption( 'c', "connections", "connections", "Sets the number of max. connections." );
		final Flag theHeaderFlag = flag( 'H', "header", "header", "Echo the HTTP-Header: When specified, then URL-Path, URL-Query and HTTP-Body are only echoed when the according switches are set." );
		final Flag theBodyFlag = flag( 'B', "body", "body", "Echo the HTTP-Body: When specified, then URL-Path, URL-Query and HTTP-Header are only echoed when the according switches are set." );
		final Flag thePathFlag = flag( 'P', "path", "path", "Echo the URL-Path: When specified, then URL-Query, HTTP-Header and HTTP-Body are only echoed when the according switches are set." );
		final Flag theQueryFlag = flag( 'Q', "query", "query", "Echo the URL-Query: When specified, then URL-Path, HTTP-Header and HTTP-Body are only echoed when the according switches are set." );
		final Flag theDateFlag = flag( 'D', "date", "date", "Include the current local date when echoing an HTTP-Request." );
		final Flag theEchoFlag = flag( 'e', "echo", "echo", "Echo the HTTP-Request back as HTTP-Response." );
		final Option<Notation> theNotationArg = enumOption( null, "notation", Notation.class, "notation", "The output notation such as: " + new VerboseTextBuilder().toString( Notation.values() ) );
		final Option<String> theUrlArg = stringOption( 'u', "url-pattern", "urlPattern", "The ant-like pattern supporting wildcards (\"*\", \"**\", \"?\") for the URL to observe. You might need to escape a trailing slash '/' with a backslash '\\', e.g. \"\\/my\\/url\\/**\"." );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();
		final Flag theDebugFlag = debugFlag();

		// @formatter:off
		final Term theArgsSyntax = cases(
			xor( theNoneArg, any( thePortArg, theMaxConnsArg, theDateFlag, theEchoFlag, thePathFlag, theQueryFlag, theHeaderFlag, theBodyFlag, theUrlArg, theNotationArg, theVerboseFlag, theDebugFlag ) ),
			xor( theHelpFlag, and( theSysInfoFlag, any( theVerboseFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "Echos HTTP messages from port", thePortArg, theVerboseFlag),
			example( "Echos HTTP messages to HTTP-Response from port", thePortArg, theEchoFlag, theVerboseFlag),
			example( "Echos just the HTTP body from port", thePortArg, theHeaderFlag, theVerboseFlag),
			example( "Echos just the HTTP body from port", thePortArg, theBodyFlag, theVerboseFlag),
			example( "Echos just the HTTP path and query from port", thePortArg, thePathFlag, theQueryFlag, theVerboseFlag),
			example( "Echos HTTP messages from port using specific notation", thePortArg, theNotationArg, theVerboseFlag),
			example( "Include local date when echoing HTTP messages from port", thePortArg, theDateFlag, theVerboseFlag),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		// final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final boolean isVerbose = theVerboseFlag.isEnabled();

		try {
			final Notation theNotation = theNotationArg.getValueOr( Notation.JSON );
			final int theMaxConnections = theMaxConnsArg.getValue() != null ? theMaxConnsArg.getValue() : -1;
			final String theUrlPattern = theUrlArg.getValueOr( "**" ).replaceAll( Pattern.quote( "\\/" ), "/" );
			final boolean isDate = theDateFlag.isEnabled();
			final boolean isEcho = theEchoFlag.isEnabled();
			final boolean isPath;
			final boolean isQuery;
			final boolean isHeader;
			final boolean isBody;

			if ( !thePathFlag.isEnabled() && !theQueryFlag.isEnabled() && !theHeaderFlag.isEnabled() && !theBodyFlag.isEnabled() ) {
				isPath = true;
				isQuery = true;
				isHeader = true;
				isBody = true;
			}
			else {
				isPath = thePathFlag.isEnabled();
				isQuery = theQueryFlag.isEnabled();
				isHeader = theHeaderFlag.isEnabled();
				isBody = theBodyFlag.isEnabled();
			}

			onRequest( theUrlPattern, ( req, res ) -> {
				PropertiesBuilder theProperties = new PropertiesBuilderImpl();
				ByteArrayOutputStream theReqBody = new ByteArrayOutputStream();
				try {
					req.getHttpInputStream().transferTo( theReqBody );
				}
				catch ( IOException ignore ) {}
				if ( isDate ) {
					echoMetaData( theProperties, "date", DateFormat.ISO_LOCAL_DATE_TIME.getFormatter().format( LocalDateTime.now() ) );
				}
				if ( isPath ) {
					echoLine( theProperties, "path", req.getUrl().getPath() );
				}
				if ( isQuery ) {
					echoHttpFields( theProperties, "query", req.getUrl().getQueryFields() );
				}
				if ( isHeader ) {
					echoHttpFields( theProperties, "header", req.getHeaderFields() );
				}
				if ( isBody ) {
					echoInputStream( theProperties, "body", new ByteArrayInputStream( theReqBody.toByteArray() ) );
				}
				System.out.println( theNotation.notationFactory.toProperties( theProperties ).toSerialized() );
				if ( isEcho ) {
					res.getHeaderFields().addAll( req.getHeaderFields() );
					res.setResponse( new ByteArrayInputStream( theReqBody.toByteArray() ) );
				}
			} ).open();

			int thePort = thePortArg.getValueOr( PortManagerSingleton.getInstance().bindAnyPort() );
			open( thePort, theMaxConnections );

			if ( isVerbose ) {
				LOGGER.info( "Port = <" + thePort + ">" );
				LOGGER.info( "URL pattern = \"" + theUrlPattern + "\"" );
				LOGGER.info( "Notation  = <" + theNotation.name() + ">" );
				LOGGER.info( "Include local date = <" + ( isDate ? "YES" : "NO" ) + ">" );
				LOGGER.info( "Show PATH = <" + ( isPath ? "YES" : "NO" ) + ">" );
				LOGGER.info( "Show QUERY = <" + ( isQuery ? "YES" : "NO" ) + ">" );
				LOGGER.info( "Show HEADER = <" + ( isHeader ? "YES" : "NO" ) + ">" );
				LOGGER.info( "Show BODY = <" + ( isBody ? "YES" : "NO" ) + ">" );
				LOGGER.info( "Echo HTTP-Request to HTTP-Response = <" + ( isEcho ? "YES" : "NO" ) + ">" );
				LOGGER.printTail();
			}
			else {
				PropertiesBuilder theProperties = new PropertiesBuilderImpl();
				theProperties.putInt( SERVER_SECTION + "/" + "port", thePort );
				theProperties.put( SERVER_SECTION + "/" + "urlPattern", theUrlPattern );
				theProperties.put( SERVER_SECTION + "/" + "notation", theNotation.name() );
				theProperties.putBoolean( SERVER_SECTION + "/" + "date", isDate );
				theProperties.putBoolean( SERVER_SECTION + "/" + "path", isPath );
				theProperties.putBoolean( SERVER_SECTION + "/" + "query", isQuery );
				theProperties.putBoolean( SERVER_SECTION + "/" + "header", isHeader );
				theProperties.putBoolean( SERVER_SECTION + "/" + "body", isBody );
				theProperties.putBoolean( SERVER_SECTION + "/" + "echo", isEcho );
				System.out.println( theNotation.notationFactory.toProperties( theProperties ).toSerialized() );
			}
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Echos the given {@link InputStream}.
	 * 
	 * @param aProperties The {@link PropertiesBuilder} into which ti 'echo'.
	 * @param aSection The prefix to be used.
	 * @param aInputStream The {@link InputStream} to be printed.
	 */
	private static void echoInputStream( PropertiesBuilder aProperties, String aSection, InputStream aInputStream ) {
		if ( aInputStream != null ) {
			final BufferedReader theBufferedReader = new BufferedReader( new InputStreamReader( aInputStream ), 1 );
			String eLine;
			int index = 0;
			try {
				while ( ( eLine = theBufferedReader.readLine() ) != null ) {
					aProperties.putValueAt( HTTP_REQUEST_SECTION + "/" + aSection, index++, eLine );
				}
			}
			catch ( IOException e ) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			}
		}
	}

	/**
	 * Echos the given {@link HttpFields}.
	 * 
	 * @param aProperties The {@link PropertiesBuilder} into which ti 'echo'.
	 * @param aSection The prefix to be used.
	 * @param aHttpFields The {@link HttpFields} to be printed.
	 */
	private static void echoHttpFields( PropertiesBuilder aProperties, String aSection, HttpFields<?> aHttpFields ) {
		if ( aHttpFields != null && aHttpFields.size() != 0 ) {
			List<String> eHeaderValue;
			for ( String eKey : aHttpFields.keySet() ) {
				eHeaderValue = aHttpFields.get( eKey );
				if ( eHeaderValue.size() == 1 ) {
					aProperties.put( HTTP_REQUEST_SECTION + "/" + aSection + "/" + eKey, eHeaderValue.get( 0 ) );
				}
				else {
					aProperties.putArray( HTTP_REQUEST_SECTION + "/" + aSection + "/" + eKey, eHeaderValue );
				}
			}
		}
	}

	/**
	 * Echos the given {@link String}.
	 * 
	 * @param aProperties The {@link PropertiesBuilder} into which ti 'echo'.
	 * @param aSection The prefix to be used.
	 * @param aLine The {@link String} to be printed.
	 */
	private static void echoLine( PropertiesBuilder aProperties, String aSection, String aLine ) {
		if ( aLine != null ) {
			aProperties.put( HTTP_REQUEST_SECTION + "/" + aSection, aLine );
		}
	}

	/**
	 * Echos the given {@link String}.
	 * 
	 * @param aProperties The {@link PropertiesBuilder} into which ti 'echo'.
	 * @param aSection The prefix to be used.
	 * @param aLine The {@link String} to be printed.
	 */
	private static void echoMetaData( PropertiesBuilder aProperties, String aSection, String aLine ) {
		if ( aLine != null ) {
			aProperties.put( META_DATA_SECTION + "/" + aSection, aLine );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private static enum Notation {

		INI(new TomlPropertiesBuilderFactory()),

		JAVA(new JavaPropertiesBuilderFactory()),

		JSON(new JsonPropertiesBuilderFactory()),

		TOML(new TomlPropertiesBuilderFactory()),

		XML(new XmlPropertiesBuilderFactory()),

		YAML(new YamlPropertiesBuilderFactory());

		ResourcePropertiesBuilderFactory notationFactory;

		private Notation( ResourcePropertiesBuilderFactory aNotationFactory ) {
			notationFactory = aNotationFactory;
		}
	}
	// /////////////////////////////////////////////////////////////////////////
	// DAEMONS:
	// /////////////////////////////////////////////////////////////////////////
}
