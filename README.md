# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***The [`funcodes-httpecho`](https://bitbucket.org/funcodez/funcodes-httpecho) repository bears some [`RESTful`](https://en.wikipedia.org/wiki/Representational_state_transfer) echo daemon.***

## Usage ##

> See the [`HTTPECHO`](https://www.metacodes.pro/manpages/httpecho_manpage) manpage for a complete user guide, basic usage instructions can be queried as follows:

```
$ ./httpecho-launcher-x.y.z.sh --help
```

## Getting started ##

To get up and running, clone the [`funcodes-httpecho`](https://bitbucket.org/funcodez/funcodes-httpecho/) repository from [`bitbucket`](https://bitbucket.org/funcodez/funcodes-httpecho)'s `git` repository. 

## How do I get set up? ##

Using `SSH`, go as follows to get the [`Maven`](https://en.wikipedia.org/wiki/Apache_Maven) [`HTTPEcho`](https://bitbucket.org/funcodez/funcodes-httpecho/) project:

```
git clone git@bitbucket.org:funcodez/funcodes-httpecho.git
```

Using `HTTPEcho`, go accordingly as follows to get the [`Maven`](https://en.wikipedia.org/wiki/Apache_Maven) [`HTTPEcho`](https://bitbucket.org/funcodez/funcodes-httpecho/) project:

```
git clone https://bitbucket.org/funcodez/funcodes-httpecho.git
```

Then you can build a [`fat-jar`](https://maven.apache.org/plugins/maven-shade-plugin/examples/executable-jar.html) and launch the application: 

```
cd funcodes-httpecho
mvn clean install
java -jar target/funcodes-httpecho-0.0.1.jar
```

## Big fat executable bash script (optional) ##

This step is optional, though when running your application under `Linux`, the following will be your friend:

> To build a big fat single executable [`bash`](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) script, take a look at the [`scriptify.sh`](https://bitbucket.org/funcodez/funcodes-httpecho/src/master/scriptify.sh) script and the [`build.sh`](https://bitbucket.org/funcodez/funcodes-httpecho/src/master/build.sh) script respectively:

```
./scriptify.sh
./target/httpecho-launcher-x.y.z.sh
```

The resulting `httpecho-launcher-x.y.z.sh` file is a big fat single executable [`bash`](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) script being launched via `./target/httpecho-launcher-x.y.z.sh`.

> Building and creating an executable bash script is done by calling `./build.sh`!

### Starting HTTPEcho ###

As described above, you may go with directly executing the `JAR` file by invoking `java -jar target/funcodes-httpecho-0.0.1.jar` or, after some doings, you may also go with the big fat executable bash script like `./target/httpecho-launcher-x.y.z.sh` to start the [`HTTPEcho`](https://bitbucket.org/funcodez/funcodes-httpecho/src/master/src/main/java/club/funcodes/httpecho/Main.java).

Either way you launch the [`HTTPEcho`](https://bitbucket.org/funcodez/funcodes-httpecho/src/master/src/main/java/club/funcodes/httpecho/Main.java) server, you will be greeted with an ASCII art banner and a log output. We assume you are in the `target` folder and use the `httpecho-launcher-x.y.z.sh` script: 

```
./httpecho-launcher-x.y.z.sh --help # Show a help message
./httpecho-launcher-x.y.z.sh        # Launch the server
```

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/funcodez/funcodes-httpecho/issues)
* Add a nifty user-interface
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
